import { Alerts } from "./layout/Alerts.js"
import { Header } from "./layout/Header.js"
import { Dashboard } from "./leads/Dashboard.js"
import { Login } from "./accounts/Login.js"
import { Register } from "./accounts/Register.js"
import store from "../store.js"

const template = document.getElementById("t-app")

class App extends HTMLElement {
  constructor() {
    super()
    this.appendChild(template.content.cloneNode(true))
    this.container = this.querySelector('.container')
  }

  switchView(viewName){
    do {
      this.container.firstChild.remove()
    }while(this.container.firstChild)
    switch(viewName){
      case '/':
        this.container.appendChild(new Dashboard());
        break;
      case '/login':
        this.container.appendChild(new Login());
        break;
      case '/register':
        this.container.appendChild(new Register());
        break;
      
    }
  }
}

customElements.define("app-component", App)

export { App }
