import routes from "../../routes.js"

const template = document.getElementById("t-register")

class Register extends HTMLElement {
  constructor() {
    super()
    this.appendChild(template.content.cloneNode(true))
    this.state = { username: "", email: "", password: "", password2: "" }
    this.state.reset = () => {
      this.state = { username: "", email: "", password: "", password2: "" }
    }
    this.form = this.querySelector("form")
  }

  connectedCallback() {
    this.form.onsubmit = this.onSubmit.bind(this)

    this.querySelectorAll("form input,textarea").forEach((input) => {
      input.onchange = this.onChange.bind(this)
    })

    routes.init.bind(this)()
  }

  onChange(e) {
    this.state[e.target.name] = e.target.value
  }
  async onSubmit(e) {
    e.preventDefault()
    // addLead(this.state, () => {
    //   this.dashboard.updateLeads()
    //   this.form.reset()
    //   this.state.reset()
    // })
    console.log('submit')
  }
}

customElements.define("register-component", Register)

export { Register }
