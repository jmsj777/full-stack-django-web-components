import routes from "../../routes.js"

const template = document.getElementById("t-login")

class Login extends HTMLElement {
  constructor() {
    super()
    this.appendChild(template.content.cloneNode(true))
    this.state = { username: "", password: "" }
    this.state.reset = () => {
      this.state = { username: "", password: "" }
    }
    this.form = this.querySelector("form")
  }

  connectedCallback() {
    this.form.onsubmit = this.onSubmit.bind(this)

    this.querySelectorAll("form input,textarea").forEach((input) => {
      input.onchange = this.onChange.bind(this)
    })

    routes.init.bind(this)()
  }

  onChange(e) {
    this.state[e.target.name] = e.target.value
  }
  async onSubmit(e) {
    e.preventDefault()
    console.log('submit')
  }
}

customElements.define("login-component", Login)

export { Login }
