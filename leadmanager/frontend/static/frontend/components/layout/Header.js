import routes from "../../routes.js"

const template = document.getElementById("t-header")

class Header extends HTMLElement {
  constructor() {
    super()
    this.appendChild(template.content.cloneNode(true))
  }
  connectedCallback() {
    routes.init.bind(this)()
  }
}

customElements.define("header-component", Header)

export { Header }
