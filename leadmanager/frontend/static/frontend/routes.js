function init() {
  let app = document.querySelector('app-component')
  this.querySelectorAll('[nav]').forEach(e => {
    e.onclick = app.switchView.bind(app, e.getAttribute('nav'))
  })
}

export default { init }