localStorage.setItem("state", JSON.stringify({}))

export default {
  setState: (newState) =>
    localStorage.setItem("state", JSON.stringify(newState)),
  getState: () => localStorage.getItem("state"),
}
